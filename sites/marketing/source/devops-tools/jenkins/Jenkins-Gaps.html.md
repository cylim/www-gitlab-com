---
layout: markdown_page
title: "Jenkins Gaps"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}

## Not a Single DevOps Application

* Jenkins is not an end to end platform for the entire DevOps Software Lifecycle

## Plugins

* Extending the native functionality of Jenkins is done through plugins. Plugins are expensive to maintain, secure, and upgrade.
    - Follow on: "Most of the organizations we talk to tell us that managing Jenkins plugins is a nightmare. Each one has it's own development life cycle, and each time a new one comes out that they need they have to re-test the whole tool chain. We've heard stories like Group A needs plugin A, and the Jenkins administrator installs it, only to find out that it depends on a newer (and incompatible) version of the library that plugin B requires, which Group B needs. Solution? Give each group their own Jenkins server? But then you end up with Jenkins sprawl (more servers to manage). Not to mention all the maintenance and testing required to all the externally integrated tools in the tool chain." (expect to see a lot of nodding during this spiel)

## High Maintenance

* Jekins requires dedicated resources with Groovy Knowledge
* Maintaining 3rd party Plug-ins can be difficult

## Increased Risk

* Integrationg 3rd party plug-ins creates exposure and introduces security risk
* Jenkins upragades may cause downtime due to pluh-in integrations

## Enterprise Readiness

* Jenkins requires 3rd party plug-ins to support analytics
* With Jenkins, audit and compliance requirements are not easy to meet.  It's not easy to track changes in a new release.

## Jenkins X 

* Jenkins X has high system requirements, Deployment limited to Kubernetes Clusters only, Only has a command line interface (at present) and Still uses Groovy

## Kubernetes Support
* From GitLab PMM - “Jenkins had to build a whole new separate project in order to work with Kubernetes. GitLab has natively adopted Kubernetes from the get-go.”

