<!-- 

If you would like to start a discussion or propose an addition
to the list of Misused Terms at GitLab, use this template to 
provide all the information.

If you'd prefer to write an MR immediately to update the YML file,
feel free to do that instead and get the proposal reviewed there.

-->

## Misused terms

<!-- 
Words to avoid using... 
Feel free to list more than one, if it's applicable
-->

* `<word>`
* `<word>`
* ...

## Alternatives

<!-- 
Use one of these instead
-->

* `<word>`
* `<word>`
* `<word>`
* `<word>`

# Reason why

<!-- 
Please elaborate (while keeping it short and succinct) about the reasons why the alternatives are better.
-->

* ...


# Review process

* [ ] Assign to a peer for review
* [ ] ...TBD

